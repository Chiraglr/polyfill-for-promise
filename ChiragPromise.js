class ChiragPromise{
    constructor(executorFn){
        this.fcount = 0;
        this.errcount = 0;
        this.args = null;
        this.fulfill = (...args)=>{
            if(this.fcount===0 && this.errcount===0){
                this.args = args;
                this.fcount++;
            }
            return;
        }
        this.reject = (...args)=>{
            if(this.fcount===0 && this.errcount===0){
                this.args = args;
                this.errcount++;
            }
            return;
        }
        try{
            executorFn(this.fulfill,this.reject);
            if(this.fcount!==0){
                return this;
            }
        }catch(err){
            this.errmessage = err;
            this.errcount++;
            return this;
        }
    }

    then(handle){
        if(this.errcount!==0){
            return this;
        }
        try{
            if(Array.isArray(this.args))
                this.args = handle(...this.args);
            else
                this.args = handle();
            return this;
        }catch(err){
            this.errmessage = err;
            this.errcount++;
            return this;
        }
    }

    catch(handle){
        if(this.errcount!==0){
            handle(this.errmessage);
        }else{
            return this;
        }
    }
}

const p = new ChiragPromise((f,err)=>{
    throw "fake error";
    f(1);
    err(2);
    return;
});

p.then((n)=>console.log("then"+n)).catch((n)=>console.log(n));